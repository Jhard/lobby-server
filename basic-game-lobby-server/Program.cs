﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace LobbyServer
{
    class Program
    {
        private static readonly int listenPort = 9000;
        static void Main(string[] args)
        {
            try
            {
                Lobby lobby = new Lobby();
                var listener = new TcpListener(IPAddress.Any, listenPort);
                listener.Start();

                while (true)
                {
                    Console.WriteLine("Waiting for connection...");

                    TcpClient client = listener.AcceptTcpClient();
                    lobby.AddClient(client);
                }
                /*listener.Stop();
                client.Close();
                Console.WriteLine(message);
                Console.ReadKey();*/
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }

    class Lobby
    {
        private static readonly string EndMessage = "end-game";
        private static readonly string ChallengeMessage = "challenge";
        private List<PlayerConnection> lobbyList;

        public Lobby()
        {
            lobbyList = new List<PlayerConnection>();

            Thread lobbyThread = new Thread(new ThreadStart(ProcessLobby))
            {
                IsBackground = true,
                Priority = ThreadPriority.Normal
            };
            lobbyThread.Start();
        }

        public void AddClient(TcpClient client)
        {
            StreamReader reader = new StreamReader(client.GetStream());

            //The Client should send it's name immediately after connection
            string name = reader.ReadLine();
            Console.WriteLine(name + " has ENTERED the lobby!");

            PlayerConnection connection = new PlayerConnection(client, name);

            Monitor.Enter(lobbyList);

            lobbyList.Add(connection);

            Monitor.Exit(lobbyList);
        }

        private void ProcessLobby()
        {
            try
            {
                while (true)
                {
                    Monitor.Enter(lobbyList);

                    List<PlayerConnection> removeList = new List<PlayerConnection>();

                    foreach (var playerConnection in lobbyList)
                    {
                        if (playerConnection.Connected)
                        {
                            string message = playerConnection.GetNextAvailableMessage();
                            if (message != null)
                            {
                                Console.WriteLine(message);
                                if (message.StartsWith(ChallengeMessage))
                                {
                                    string opponent = message.Substring(message.IndexOf(":") + 1);
                                    foreach (var player in lobbyList)
                                    {
                                        if (player.Name == opponent)
                                        {
                                            playerConnection.Writer.WriteLine("game-start:host");
                                            player.Writer.WriteLine("game-start:client");
                                            removeList.Add(player);
                                            removeList.Add(playerConnection);
                                            ThreadPool.QueueUserWorkItem(
                                                RunGame,
                                                new Tuple<PlayerConnection, PlayerConnection>(playerConnection, player)
                                                );
                                            break;
                                        }
                                    }
                                }
                                if (message == "get-lobby")
                                {
                                    string playerList = "lobby:";
                                    if (lobbyList.Count > 1)
                                    {
                                        foreach (var player in lobbyList)
                                        {
                                            if (player != playerConnection)
                                            {
                                                playerList += player.Name + ";";
                                            }
                                        }
                                    }
                                    playerConnection.Writer.WriteLine(playerList);
                                }
                            }
                        }
                        else
                        {
                            removeList.Add(playerConnection);
                        }
                    }

                    while (removeList.Count != 0)
                    {
                        lobbyList.Remove(removeList.First());
                        Console.WriteLine(removeList.First().Name + " has EXITED the lobby!");
                        removeList.RemoveAt(0);
                    }

                    Monitor.Exit(lobbyList);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void RunGame(object obj)
        {
            var players = obj as Tuple<PlayerConnection, PlayerConnection>;
            var player1 = players.Item1;
            var player2 = players.Item2;

            try
            {
                bool done = false;
                while (!done)
                {
                    RelayMessage(player1, player2);
                    RelayMessage(player2, player1);

                    if (!player1.Connected || !player2.Connected)
                    {
                        player1.Close();
                        player2.Close();
                        done = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void RelayMessage(PlayerConnection player1, PlayerConnection player2)
        {
            if (!player1.Connected || !player2.Connected)
            {
                return;
            }
            string message = player1.GetNextAvailableMessage();

            if (message == EndMessage)
            {
                player1.Close();
                player2.Close();
            }
            else if (message != null)
            {
                player2.Writer.WriteLine(message);
            }
        }

        private class PlayerConnection
        {
            private DateTime lastPing;
            private DateTime lastResponce;
            private readonly int pingInterval = 10000;
            private readonly int pingWait = 10000;
            private bool waitingForPing = false;

            public TcpClient Client { get; set; }
            public bool Connected { get; set; } = true;
            public string Name { get; set; }
            public StreamReader Reader { get; set; }
            public StreamWriter Writer { get; set; }

            public PlayerConnection(TcpClient client, string name)
            {
                Name = name;
                Client = client;
                Writer = new StreamWriter(client.GetStream());
                Reader = new StreamReader(client.GetStream());
                Writer.AutoFlush = true;

                lastPing = DateTime.Now;
            }

            public string GetNextAvailableMessage()
            {
                if (!Connected)
                {
                    return null;
                }

                try
                {
                    if (Client.Available > 0)
                    {
                        lastResponce = DateTime.Now;
                        waitingForPing = false;
                        
                        return Reader.ReadLine();
                    }
                    else
                    {
                        if (waitingForPing)
                        {
                            if (TimeSince(lastPing) > pingWait)
                            {
                                Close();
                            }
                        }
                        else
                        {
                            if (TimeSince(lastResponce) > pingInterval)
                            {
                                waitingForPing = true;
                                Writer.WriteLine("ping");
                                lastPing = DateTime.Now;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Close();
                }

                return null;
            }

            private double TimeSince(DateTime time)
            {
                return (DateTime.Now - time).TotalMilliseconds;
            }

            public void Close()
            {
                if (Connected)
                {
                    Connected = false;
                    Reader.Close();
                    Writer.Close();
                    Client.Close();
                }
            }
        }
    }

}
